package com.volidar.cartoy.shared.jsonprotocol

//TODO This is not used at all -> remove?
sealed trait WebToCore
case object Initialize extends WebToCore

sealed trait WebFromCore
case object Initialized extends WebFromCore //Not used
