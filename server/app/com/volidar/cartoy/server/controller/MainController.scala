package com.volidar.cartoy.server.controller

import com.typesafe.scalalogging.LazyLogging
import play.api.data.Form
import play.api.data.Forms._
import play.api.mvc.{Action, Controller}

object MainController extends Controller with LazyLogging {
  def index = Action {
    Ok(views.html.index())
  }

  val botForm = Form(
    tuple(
      "d" -> number,
      "s" -> number
    )
  )

  def bot = Action { implicit request =>
    val (d, s) = botForm.bindFromRequest.get
    logger.info(s"bot: d=$d; s=$s")
    Ok("OK").as("text/plain")
  }
}
