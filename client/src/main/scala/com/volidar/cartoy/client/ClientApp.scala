package com.volidar.cartoy.client

import scala.scalajs.js

object ClientApp extends js.JSApp {
  def main(): Unit = new ClientUi()
}
