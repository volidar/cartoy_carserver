package com.volidar.cartoy.client.common.ui

import com.volidar.cartoy.client.common.logging.LazyLogging
import org.scalajs.jquery.{JQuery, JQueryEventObject}

import scala.scalajs.js

class Slider(selectedSlider: JQuery, initialMax: Int = 255, initialValue: Int = 128) extends LazyLogging {
  val dynamic = selectedSlider.asInstanceOf[js.Dynamic]
  dynamic.slider()

  max = initialMax
  value = initialValue

  def max: Int = dynamic.slider("option", "max").asInstanceOf[Number].intValue()
  def max_=(newValue: Int) = dynamic.slider("option", "max", newValue)
  def value: Int = dynamic.slider("option", "value").asInstanceOf[Number].intValue()
  def value_=(newValue: Int) = dynamic.slider("option", "value", newValue)
  def enable(enabled: Boolean) = {
    dynamic.slider(if(enabled) "enable" else "disable")
    this
  }
  def slide(handler: (JQueryEventObject, js.Dynamic) => js.Any) = {
    dynamic.on("slide", handler)
    this
  }
}
