package com.volidar.cartoy.client.common

object AverageValue {
  val empty = AverageValue(0, 0)
  def first(data: Int) = AverageValue(data, 1)
}
case class AverageValue private (total: Int, count: Int) {
  def value: Int = if(count == 0) 128 else total / count
  def +(data: Int) = new AverageValue(total + data, count + 1)
}
