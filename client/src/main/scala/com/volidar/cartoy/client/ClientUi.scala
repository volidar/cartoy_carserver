package com.volidar.cartoy.client

import com.volidar.cartoy.client.common.AverageValue
import com.volidar.cartoy.client.common.logging.LazyLogging
import com.volidar.cartoy.client.common.ui.Slider
import com.volidar.cartoy.client.communication.HttpPostClient
import org.scalajs.dom.window
import org.scalajs.jquery._

import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import scala.scalajs.js
import scala.util.{Failure, Success}

class ClientUi extends LazyLogging {
  var direction: AverageValue = AverageValue.empty
  var speed: AverageValue = AverageValue.empty

  val speedSlider = new Slider(jQuery("#speedSlider"))
  speedSlider.slide((_, _) => speed += speedSlider.value)

  val directionSlider = new Slider(jQuery("#directionSlider"))
  directionSlider.slide((_, _) => direction += directionSlider.value)

  window.addEventListener("devicemotion", (event: js.Any) => {
    speedSlider.enable(false)
    directionSlider.enable(false)

    val dynEvent = event.asInstanceOf[js.Dynamic]
    val x = parseDouble(dynEvent.accelerationIncludingGravity.x)
    val y = parseDouble(dynEvent.accelerationIncludingGravity.y)
    val z = parseDouble(dynEvent.accelerationIncludingGravity.z)

    val curSpeed = math.min(math.max(0, math.asin(z / math.sqrt(x * x + y * y + z * z)) / math.Pi * 4 - 0.4) * 256, 255).toInt
    speedSlider.value = curSpeed
    speed += curSpeed

    val curDirection = if(x == 0.0) 128 else {
      val normalizedY = if(math.abs(y) < math.abs(x)) y else math.abs(x) * math.signum(y)
      math.max(-128, math.min((math.atan(normalizedY / x) / math.Pi * 4 * 256).toInt, 127)) + 128
    }
    directionSlider.value = curDirection
    direction += curDirection
  })

  def parseDouble(value: js.Any): Double = value match {
    case number: Number => number.doubleValue()
    case _ => 0.0
  }

  window.setInterval(sendRequest _, 50)

  private def sendRequest() = {
    val directionValue = direction.value - 128
    val speedValue = speed.value - 128

    direction = AverageValue.first(direction.value) //reset values, preserve last one
    speed = AverageValue.first(speed.value)

    val requestBody = s"d=$directionValue&s=$speedValue"
    HttpPostClient.send(requestBody).onComplete {
      case Success(responseBody) => logger.debug(s"'$requestBody' => '$responseBody'")
      case Failure(cause) => logger.error(s"s'$requestBody' =X", cause)
    }
  }
}
