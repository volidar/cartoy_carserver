package com.volidar.cartoy.client.communication

import com.volidar.cartoy.client.common.logging.LazyLogging
import org.scalajs.jquery.{JQueryXHR, jQuery}

import scala.concurrent.{Future, Promise}
import scala.scalajs.js
import org.scalajs.jquery.JQueryAjaxSettings

object HttpPostClient extends LazyLogging {
  def relativePostUrl = "/bot"

  def send(messageToServer: String): Future[String] = {
    val promise = Promise[String]

    def handleAjaxError(jqXHR: JQueryXHR, textStatus: String, errorThrown: String) = {
      logger.error(s"'$messageToServer' => $textStatus:\n$errorThrown")
      promise.failure(new Exception(textStatus))
    }
    def handleAjaxSuccess(data: js.Any, textStatus: String, jqXHR: JQueryXHR) = {
      val messageFromServer = jqXHR.responseText
      logger.error(s"'$messageToServer' => '$messageFromServer'")
      promise.success(messageFromServer)
    }

    jQuery.ajax(js.Dynamic.literal(
      data = messageToServer,
      url = relativePostUrl, contentType = "application/x-www-form-urlencoded", `type` = "POST",
      success = handleAjaxSuccess _, error = handleAjaxError _).asInstanceOf[JQueryAjaxSettings])

    promise.future
  }
}
