package com.volidar.cartoy.client.common.logging

trait LazyLogging { self => //DOIT replace with slogging
  val logger = new Logger(self.getClass)
}

class Logger private[logging] (category: String) {
  def this(cls: Class[_]) = this(cls.getSimpleName)

  def error(msg: Any, ex: Throwable = null) = log("ERROR", msg)
  def warn(msg: Any) = log("WARN", msg)
  def info(msg: Any) = log("INFO", msg)
  def debug(msg: Any) = log("DEBUG", msg)

  private def log(level: String, msg: Any, ex: Throwable = null) = println(f"$level%-5s $msg%s")
}
