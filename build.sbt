import sbt._
import sbt.Project.projectToRef

val zScalaVersion = "2.11.8"
val zAkkaVersion = "2.4.2"
val zUPickleVersion = "0.4.4"

addCommandAlias("ep", "~exported-products")
addCommandAlias("ept", "~;exported-products;test:exported-products")

scalacOptions in ThisBuild ++= Seq("-deprecation", "-feature", "-unchecked")

lazy val clients = Seq(client)

lazy val server = (project in file("server")).settings(
  scalaVersion := zScalaVersion,
  scalaJSProjects := clients,
  pipelineStages := Seq(scalaJSProd, gzip),
  resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases",
  libraryDependencies ++= Seq(
    "org.webjars" % "jquery" % "1.12.4", //DOIT 3.1.1
    "org.webjars" % "jquery-ui" % "1.12.1",
    "com.lihaoyi" %% "upickle" % zUPickleVersion,
    "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0",
    "ch.qos.logback" % "logback-core" % "1.1.8", //DOIT remove it later
    "ch.qos.logback" % "logback-classic" % "1.1.8"
  )
).enablePlugins(PlayScala).
  aggregate(clients.map(projectToRef): _*).
  dependsOn(sharedJvm)

lazy val client = (project in file("client")).settings(
  scalaVersion := zScalaVersion,
  persistLauncher := true,
  persistLauncher in Test := false,
  libraryDependencies ++= Seq(
    "org.scala-js" %%% "scalajs-dom" % "0.9.1",
    "com.vmunier" %% "play-scalajs-scripts" % "0.5.0",
    "be.doeraene" %%% "scalajs-jquery" % "0.9.1",
    "com.lihaoyi" %%% "upickle" % zUPickleVersion //DOIT add slogging
  )
).enablePlugins(ScalaJSPlugin, ScalaJSPlay).
  dependsOn(sharedJs)

lazy val shared = (crossProject.crossType(CrossType.Pure) in file("shared")).
  settings(scalaVersion := zScalaVersion).
  jsConfigure(_ enablePlugins ScalaJSPlay)

lazy val sharedJvm = shared.jvm
lazy val sharedJs = shared.js

// loads the Play project at sbt startup
onLoad in Global := (Command.process("project server", _: State)) compose (onLoad in Global).value
