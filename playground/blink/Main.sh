#!/bin/sh
exec scala -classpath "lib/pi4j-core.jar:lib/pi4j-device.jar:lib/pi4j-gpio-extension.jar:lib/pi4j-service.jar" "$0" "$@"
!#

import com.pi4j.io.gpio._

object Main {
	val gpio: GpioController = GpioFactory.getInstance

	def main(args: Array[String]): Unit = {
		val led = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_04, "LED", PinState.LOW)
		while(true) {
			led.low
			Thread.sleep(1000)
			led.high
			Thread.sleep(1000)
		}
		throw new Exception("end")
	}
}

Main.main(args)