import com.pi4j.io.gpio._

object Main {
	val gpio: GpioController = GpioFactory.getInstance

	def main(args: Array[String]): Unit = {
		val led = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_04, "LED", PinState.LOW)
		while(true) {
			led.low
			Thread.sleep(1000)
			led.high
			Thread.sleep(1000)
		}
		throw new Exception("end")
	}
}